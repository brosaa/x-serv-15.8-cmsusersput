from django.db import models

# Create your models here.
class contenido (models.Model):
    clave = models.CharField(max_length=30)
    valor = models.CharField(max_length=60)
    def __str__(self):
        return str(self.id) + ": " + self.clave + " -- " + self.valor