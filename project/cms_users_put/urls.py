from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.content),
    path('loggedIn', views.loggedIn),
    path('logout', views.loggedOut),
    path('<str:llave>', views.get_content),
]