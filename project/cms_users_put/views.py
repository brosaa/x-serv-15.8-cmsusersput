from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseNotFound
from .models import contenido
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.template import loader

formulario = """
<p>
<form action= "" method="POST">
    valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def content (request):
    content = contenido.objects.all()
    template = loader.get_template('cms_users_put/contenidos.html')
    context = {
        'content_list':content,
    }
    return HttpResponse (template.render(context,request))

@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT" or request.method == "POST":
        valor = request.body.decode('utf-8')
        if request.user.is_authenticated:  # Si se encuentra autenticado puede camabiar contenido
            try:
                c = contenido.objects.get(clave=llave)
                c.valor = valor
            except contenido.DoesNotExist:
                c = contenido(clave=llave, valor=valor)
            c.save()  # Guardamos el contenido en la base de datos

    try:
        clave = contenido.objects.get(clave=llave)
        respuesta = "La clave " + llave + " tiene asignado " + clave.valor
    except contenido.DoesNotExist:
        if request.user.is_authenticated:    #Si se encuentra autenticado puede añadir contenido
            respuesta = "Tu nombre de usuario es " + request.user.username + "<br>"
            respuesta += formulario
        else:
            respuesta = "Tienes que estar autenticado/a. <a href='/login'>¡Registrate!</a>"
    return HttpResponse(respuesta)

@csrf_exempt
def loggedIn (request):
    if request.user.is_authenticated:
        respuesta = "Estás registrado/a como " + request.user.username
    else:
        respuesta = "Tienes que estar registrado/a <a href='/login'>¡Registrate!</a>"
    return HttpResponse (respuesta)

@csrf_exempt
def loggedOut (request):
    logout(request)
    return redirect('/cms/')
